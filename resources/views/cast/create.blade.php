@extends('adminlte.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">
            Add New Casts
        </h3>
    </div>
    <form role="form p-5" method="POST" action="/cast">
        @csrf
        <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama"  value="{{old('nama', '')}}"" placeholder="Masukkan Nama">
            @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
        </div>
        <div class="form-group">
            <label for="Umur">Umur</label>
            <input type="number" class="form-control" id="umur" placeholder="Umur" name="umur" value="{{old('umur', '')}} ">
            @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label><br>
            <textarea type="file" id="bio" name="bio"  value="{{old('bio', '')}}""></textarea>
        </div>
        </div>
        
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    
</div>
@endsection