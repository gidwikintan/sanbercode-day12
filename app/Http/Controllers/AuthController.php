<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function data(Request $request)
    {
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        $fullName = $firstName . " " . $lastName;



        return view('welcome', [
            'fullName' => $fullName
        ]);
    }
}
