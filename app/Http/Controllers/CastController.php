<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('cast.index', compact('casts'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
            'bio' => 'required|max:255',
        ]);
        $query = DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('cast')->with('success', 'Post Berhasil Disimpan');
    }
    public function show($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('cast.show', compact('casts'));
    }


    public function edit($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('cast.edit', compact('casts'));
    }


    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|',
            'umur' => 'required|',
            'bio' => ''
        ]);
        $query = DB::table('casts')->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('cast');
    }

    public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('cast');
    }
}
